# Homeworks
## Rules for submitting the homework:

Please provide both your code in this Rmd document and an html file for your final write-up. Please pay attention to the clarity and cleanness of your homework.

The teaching fellows will grade your homework and give the grades with feedback through canvas within one week after the due date. Some of the questions might not have a unique or optimal solution. TAs will grade those according to your creativity and effort on exploration.


## 6/7  11:59p.m HW1 Due 
## 6/14 11:59p.m HW2 Due 
## 6/21 11:59p.m HW3 Due 
## 6/28 11:59p.m HW4 Due 
## 7/5  11:59p.m HW5 Due 

## Submission link: http://cistrome.org/~changxin/cgi-bin/
